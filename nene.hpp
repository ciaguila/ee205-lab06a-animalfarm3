///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.hpp
/// @version 1.0
///
/// Exports data about all nene
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "bird.hpp"

using namespace std;


namespace animalfarm {

class Nene : public Bird {
public:
   string tag; //This tag is generated to pass data from the constructor to the speak function

   Nene( string tagID, enum Color newColor, enum Gender newGender ); //Nene constructor with tagID string specified

   virtual const string speak(); //a unique speak is declared for Nenes

   void printInfo();

};

} // namespace animalfarm

