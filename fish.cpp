///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file fish.cpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>

#include "fish.hpp"

using namespace std;

namespace animalfarm {

void Fish::printInfo() {

   Animal::printInfo();
   cout << "   Scale Color = [" << colorName( scaleColor ) << "]" << endl; // sends the scaleColor enum to the color name string return, and returns the correct color string
   cout << "   Favorite Temperature= [" << favoriteTemp << "]" << endl; // returns the favorite temperature stored within the fish data
}


const string Fish::speak() {
   return string( "Bubble bubble" );
}



} // namespace animalfarm

