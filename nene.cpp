///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author @todo Christopher Aguilar
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @toda Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene(string tagID, enum Color newColor, enum Gender newGender ) {
   gender = newGender;
   species = "Branta sandvicensis";    /// Hardcoded due to all Nene being the same species
   featherColor = newColor;       /// Feather color passed in from the constructor
   isMigratory = true; /// Migratory status is true for all nene
   
   tag = tagID; /// This tag holds the tagID passed from the constructor for use in the printInfo

}

//This returns out the speak specifically for the Nene, and overrides the Bird speak
const string Nene::speak() {
   return string( "Nay, nay" );
}
//This returns the tag ID and then forwards to the Bird class to print out rest of data
void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << tag << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm




