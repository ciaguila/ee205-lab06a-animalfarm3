///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Christopher Aguilar
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   Feb 2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool isNative, enum Color newColor, enum Gender newGender ) {
   gender = newGender;        
   species = "Fistularia chinensis";    /// Hardcoded. all nunu are the same species (this is a is-a relationship)
   scaleColor = newColor;  
   favoriteTemp = 80.6;       /// All nunu have the same favorite temperature
   native = isNative;         /// Get from the constructor... not all nunu are native to hawaii

}


/// Print our Nunu and native first... then print whatever information Fish holds.
void Nunu::printInfo() {
   cout<< "Nunu" << endl;
   cout << "   Is native = [" << boolalpha << native << "]" << endl; // This uses the boolalpha command to generate true or false from 1 and 0
   Fish::printInfo();
}

} // namespace animalfarm

