///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <random>
#include "animal.hpp"

using namespace std;

namespace animalfarm {
	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
	  switch (color) {
      case RED:    return string("Red"); break;
      case BLACK:  return string("Black"); break;
      case WHITE: return string("White"); break;
      case SILVER:    return string("Silver"); break;
      case YELLOW:  return string("Yellow"); break;
      case BROWN: return string("Brown"); break;

   }

   return string("Unknown");
};



Animal::Animal() {
   cout << ".";
}

Animal::~Animal() {
   cout << "x ";
}

const Gender Animal::getRandomGender(){
   
   random_device rd;
      mt19937 mt(rd());
      uniform_real_distribution<double> dist(0, 2);
   int l = dist(mt);
   //printf("DEBUG: Entered getRandomGender %d \n", l);

   switch(l) {
      case 0: return MALE; break;
      case 1: return FEMALE; break;
   }
return UNKNOWN;

};

const Color Animal::getRandomColor(){
      random_device rd;
      mt19937 mt(rd());
      uniform_real_distribution<double> dist(0, 6);
      int l = dist(mt);

     // printf("DEBUG: Entered getRandomColor %d \n", l);

      switch(l) {
         case 0: return RED; break;
         case 1: return BLACK; break;
         case 2: return WHITE; break;
         case 3: return SILVER; break;
         case 4: return YELLOW; break;
         case 5: return BROWN; break;
            }
      return RED;
 };


const bool Animal::getRandomBool(){
   random_device rd;
      mt19937 mt(rd());
      uniform_real_distribution<double> dist(0, 2);

   int l = dist(mt);
   //printf("DEBUG: Entered getRandomBool %d \n", l);

   switch(l) {
      case 0: return true; break;
      case 1: return false; break;
   }
   return true;

};

const float Animal::getRandomWeight(const float from, const float to){

   
   srand(time(NULL));
   float d = to-from; //find range of weights
   float wrangenum = ((float)rand()/(float)(RAND_MAX))*d; //pick a random number between that the range
   float finalweight = from + wrangenum; //add the ranged number to the minimum to get actual value
   //printf("DEBUG: Entered getRandomWeight %f \n", finalweight);

   return finalweight;

}

const string Animal::getRandomName(){
   random_device rd;
      mt19937 mt(rd());
      uniform_real_distribution<double> dist(0, 6);

   int range = dist(mt);
   int length = 4 + range;
   
   //printf("DEBUG: Entered getRandomName %d \n", length);


   char randomName[length];

   int v = rand() %('Z' - 'A');

   randomName[0] = 'A' + v;

   for(int i=1; i<length; i++)
   {
    v = rand() %('z'-'a');
    randomName[i] = 'a' + v;
   };

   return randomName;

}


} // namespace animalfarm
