// Animal Farm 3
//
// Christopher Aguilar
//
// March 2021

#include <iostream>
#include <array>
#include <list>
#include <random>

#include <cstdlib>
#include <ctime>

#include "af.hpp"
#include "animal.hpp"
#include "mammal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "fish.hpp"
#include "bird.hpp"

using namespace std;
using namespace animalfarm;

int main() {

   srand(time(NULL));
   cout << "Welcome to Animal Farm 3" << endl;
   
   
   array<Animal*, 30> animalArray;
   animalArray.fill(0);
   for(int i = 0; i <25; i++){
      animalArray[i] = AnimalFactory::getRandomAnimal();
      //animalArray[i]->printInfo();
   }
   
   cout << "\n" << "Array of Animals:" << endl;
   cout << "Is it empty:" << boolalpha << animalArray.empty() << endl;
   cout << "Number of elements:" << animalArray.size() << endl;
   cout << "Max Size:" << animalArray.max_size() << endl;
   
   /*
   for(Animal* animal: animalArray) {
      if( animal == 0){
         break; }
      
      cout << animal->speak() << endl;
   }
   */
   int s = animalArray.size();
   for( int i = 0; i < s; i++){
      if(animalArray[i] == 0) { break; }
      cout << animalArray[i]->speak() << endl;
      //animalArray[i]->printInfo();
   }

   for(Animal* animal: animalArray) {
      if(animal == 0){ break; }
      animal->~Animal();
   }
   
   cout << endl;

   list<Animal*> animalList;
   for(int i = 0; i < 25; i++) {
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }
   cout << "\n" << "List of Animals:" << endl;
   cout << "Is it empty:" << boolalpha << animalList.empty() << endl;
   cout << "Number of Elements:" << animalList.size() << endl;
   cout << "Max size:" << animalList.max_size() << endl;
   
   for(Animal* animal: animalList)
      {
         cout << animal->speak() << endl;
      }
   for(Animal* animal: animalList)
   {
      animal->~Animal();
   }
   
   cout << endl;


};
