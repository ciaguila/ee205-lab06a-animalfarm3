# Compiler for AnimalFarm3
#
# Chris Aguilar
# March 2021


all: main

main.o:  animal.hpp main.cpp af.hpp 
	g++ -c main.cpp

af.o: af.hpp animal.hpp af.cpp 
	g++ -c af.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp

mammal.o: mammal.hpp mammal.cpp animal.hpp
	g++ -c mammal.cpp

fish.o: fish.cpp fish.hpp animal.hpp
	g++ -c fish.cpp

bird.o: bird.cpp bird.hpp animal.hpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp mammal.hpp
	g++ -c cat.cpp

dog.o: dog.cpp cat.hpp mammal.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp fish.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp fish.hpp
	g++ -c aku.cpp

palila.o: palila.cpp palila.hpp bird.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp bird.hpp
	g++ -c nene.cpp

main: main.cpp *.hpp main.o af.o  animal.o mammal.o cat.o dog.o fish.o bird.o nunu.o aku.o palila.o nene.o
	 g++ -o main main.o af.o animal.o mammal.o cat.o dog.o fish.o bird.o nunu.o aku.o palila.o nene.o

clean:
	rm -f *.o main
