
#pragma once

using namespace std;

#include <string>
#include "animal.hpp"

namespace animalfarm {

class AnimalFactory {

   public:
   static Animal* getRandomAnimal();
};


} // namespace animalfarm

